http = require('http');
var express    = require('express');
var app = express();
var server = http.createServer(app);
var p2pserver = require('socket.io-p2p-server').Server;
var io = require('socket.io').listen(server);
var path = require('path');

var port = process.env.PORT || 3000;        // set our port

io.use(p2pserver);

function connectedUserList()
{
	this.connectedUsers = {};
	this.toString = function() {
		var string = "";
		var connectedUsersString = "";
		for(var ip in this.connectedUsers){
			string += "<p>"+ this.connectedUsers[ip] + "</p>";
			connectedUsersString += "[" + ip + " " + this.connectedUsers[ip] + "]\t";
		}
		console.log(connectedUsersString);
		return string;
	};
	this.updateUser = function(username, ip){
		this.connectedUsers[ip] = username;
	};
	this.addUser = this.updateUser;
	this.removeUser = function(ip){
		delete this.connectedUsers[ip];
	};
}
var connectedUserList = new connectedUserList();

io.on('connection', function (socket) {
	socket.on('peer-msg', function (data) {
		socket.broadcast.emit('peer-msg', data);
	});
	connectedUserList.addUser("user", socket.request.connection.remoteAddress);
	io.emit('update', connectedUserList.toString());
	socket.on('message', function(message){
		connectedUserList.updateUser(message, socket.request.connection.remoteAddress);
		io.emit('update', connectedUserList.toString());
	});
	socket.on('disconnect', function(){
		connectedUserList.removeUser(socket.request.connection.remoteAddress);
	});
});


// ROUTES FOR OUR API
// =============================================================================
var router = express.Router();              // get an instance of the express Router

// test route to make sure everything is working (accessed at GET http://localhost:8080/api)
router.get('/', function(req, res) {
	res.sendFile(path.join(__dirname + "/index.html"));
});
router.get('/chat/([0-9]+)', function(req, res){
	res.sendFile(path.join(__dirname + "/chat/chat.html"));
});

// more routes for our API will happen here

// REGISTER OUR ROUTES -------------------------------
// all of our routes will be prefixed with /api
app.use('/', router);
app.use('/', express.static(path.join(__dirname, '/')));

// START THE SERVER
// =============================================================================
server.listen(port);
console.log('Magic happens on port ' + port);