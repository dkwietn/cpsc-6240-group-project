function symmetricEncrypt(message)
{
	var value;
	var encryptedMessage = "";
	for( var i = 0; i < message.length; i += 1 )
	{
		value = message.charCodeAt(i);
		value = (value + symmetricKey) % 256;
		encryptedMessage += String.fromCharCode(value);
	}
	return encryptedMessage;
}

function symmetricDecrypt(encryptedMessage)
{
	var value;
	var message = "";
	for( var i = 0; i < encryptedMessage.length; i += 1 )
	{
		value = encryptedMessage.charCodeAt(i);
		value = (value - symmetricKey) % 256;
		message += String.fromCharCode(value);
	}
	return message;
}

function initCryptoSystem()
{
	privateCryptoSystem = new JSEncrypt({default_key_size: 1024});
	sendMessage(privateCryptoSystem.getPublicKey(), "pubkey");
	symmetricKey = Math.floor(Math.random() * 255) + 1;
	console.log("got private key");
}

function getKeyRemote(keyMessage)
{	
	if( !secureConnection )
	{
		peerCryptoSystem = new JSEncrypt();
		peerCryptoSystem.setPublicKey(keyMessage);
		sendMessage(privateCryptoSystem.getPublicKey(), "pubkey");
		console.log("got public key");
		sendMessage(peerCryptoSystem.encrypt(symmetricKey.toString()), "symkey");
		secureConnection = true;
	}
}

function getSymmetricKey(keyMessage)
{
	if( !gotSymmetricKey )
	{
		symmetricKey = (symmetricKey + parseInt(privateCryptoSystem.decrypt(keyMessage))) % 255 + 1;
		console.log("got symmetric key");
		gotSymmetricKey = true;
	}
}

function encrypt( message )
{
	var encryptedMessage = message;
	if( secureConnection )
	{
		encryptedMessage = peerCryptoSystem.encrypt(encryptedMessage);
	}
	if( gotSymmetricKey )
	{
		encryptedMessage = symmetricEncrypt(encryptedMessage);
	}
	return encryptedMessage;
}

function decrypt( encryptedMessage )
{
	var decryptedMessage = encryptedMessage;
	if( gotSymmetricKey )
	{
		decryptedMessage = symmetricDecrypt(encryptedMessage);
	}
	if( secureConnection )
	{
		decryptedMessage = privateCryptoSystem.decrypt(decryptedMessage); 
	}
	return decryptedMessage;
}