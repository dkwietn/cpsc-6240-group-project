function initServerConnection()
{
	serverConnection = io();
	serverConnection.on('update', receiveFromServer);
}

function sendToServer(message) 
{
	serverConnection.emit('message', message);
}

function receiveFromServer(message) 
{
	document.getElementById("connections").innerHTML = message;
}

function closeServerConnection()
{
	if( serverConnection != null )
		serverConnection.close();
	var server = document.getElementById("server");
	server.parentElement.removeChild(server);
}

function changeUsername(newUsername)
{
	if(newUsername.length > 0)
	{
		username = newUsername;
		if(serverConnection != null)
			sendToServer(username);
	}
}

function initPeerConnection()
{
    peerConnection.on('peer-msg', function (data) {
		receiveMessage(data.textVal);
    });
	sendMessage((Math.random()).toString(), "peerID")
	initCryptoSystem();	
}

function kbdSendMessage(e)
{
	if(e.keyCode == 13 && !e.shiftKey) 
	{
		sendTextMessage();
		e.preventDefault();
	}
}

function sendTextMessage()
{
		var message = formatMessage(username, sendMessageText.value);
		addMessage(message);
		sendMessageText.value = '';
		sendMessageText.focus();
		sendMessage(message, "text");
}

function sendMessage( message, type )
{
	if( type == "text" )
	{
		message = encrypt(message);
		message = "1" + message;
	}
	else if( type == "pubkey" )
	{
		message = "2" + message;
	}
	else if( type == "symkey" )
	{
		message = "3" + message;
	}
	else if( type == "peerID" )
	{
		message = "4" + message;
	}
	else
	{
		console.log("Invalid Message Sent: " + type);
	}
	peerConnection.emit('peer-msg', {textVal: message});
}

function receiveMessage(message)
{
	/* message types 
		1 = normal message
		2 = public key
		3 = symmetric key
		4 = peer id
	*/
	type = message.substr(0,1);
	message = message.substr(1, message.length);
	if( type == "1")
	{
		message = decrypt(message);
		addMessage(message);
	}
	else if( type == "2")
	{
		if( !secureConnection )
		{
			getKeyRemote(message);
		}
	}
	else if( type == "3")
	{
		if( !gotSymmetricKey )
		{
			getSymmetricKey(message);
		}
	}
	else if( type == "4")
	{
		if( peerID != message )
		{
			peerID = message;
			secureConnection = false;
			gotSymmetricKey = false;
			initCryptoSystem();
		}
	}
	else
	{
		console.log("Invalid Message Received: " + message);
	}
}

function formatMessage(user, message)
{
	var text = message.trim();
	if( text.length > 0 ) 
	{
		padding = "";
		for( var i = 0; i < user.length + 2; i++ )
			padding += " ";
		text = user + ": " + text.replace(/\n/g,"\n" + padding) + "\n";
	}
	return text;
}

function addMessage(text)
{
	if( text == null )
		text = "*** An Error Occurred, Please Refresh Page ***\n";
	if( text.length > 0 )
		recvMessageArea.value += text;
	recvMessageArea.scrollTop = recvMessageArea.scrollHeight;
}